package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {

    //Properties
    //indicates primary key
    @Id
    //auto increment
    @GeneratedValue
    private Long id;

    //Class properties that represents table column in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    //Establishes the relationship of the property to the "post" model
    @ManyToOne
    //sets the relationship to this property and user_id column in the database to the primary key of the user model
    @JoinColumn(name="user_id", nullable=false)
    private User user;


    //Constructors
    //Default contructors are required when retrieving data from the database or class
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //Getters and Setters
    public String getTitle(){
        return title;
    }

    public void setTitle(String Title){
        this.title=Title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String Content){
        this.content=Content;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user=user;
    }
}
