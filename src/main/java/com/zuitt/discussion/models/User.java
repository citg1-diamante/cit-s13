package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "user")
public class User {

    //Properties
    //indicates primary key
    @Id
    @GeneratedValue
    private Long id;

    //Class properties that represents table column in a relational database are annotated as @Column
    @Column
    private String username;

    @Column
    private String password;

    //establishes the relationship of the properties
    @OneToMany(mappedBy="user")
    //Prevent infinite recursion with bidirectional relationship.
    @JsonIgnore
    //"Set" class is a collection that contains no duplicate elements.
    private Set<Post> posts;

    //Constructors
    //Default contructors are required when retrieving data from the database or class
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //Getters and Setters
    public Long getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String Username){
        this.username = Username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String Password){
        this.password = Password;
    }

    public Set<Post> getPosts(){
        return posts;
    }

    public void setPosts(Set<Post> posts){
        this.posts=posts;
    }
}
