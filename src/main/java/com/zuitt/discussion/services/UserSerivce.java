package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;

import java.util.Optional;

public interface UserSerivce {

    //Optional defines if the method may or may not return an object of the User class.
    Optional<User> findByUsername(String username);


}
